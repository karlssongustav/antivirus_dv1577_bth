#
#Gustav Karlsson - gustav.ae.karlsson@gmail.com
#Claes Nyman - clny17@student.bth.se
#

COMPILER := g++
COMPILER_ARGS := -std=c++11 -Wall
BIN_NAME := antivirus

all: AntiVirus.o
	$(COMPILER) $(COMPILER_ARGS) AntiVirus.o Main.cpp -o $(BIN_NAME)

AntiVirus.o : AntiVirus.cpp
	$(COMPILER) $(COMPILER_ARGS) -c AntiVirus.cpp -o AntiVirus.o

clean:
	rm -f AntiVirus.o $(BIN_NAME)
