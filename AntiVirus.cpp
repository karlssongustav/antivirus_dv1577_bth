/*
Gustav Karlsson - gustav.ae.karlsson@gmail.com
Claes Nyman - clny17@student.bth.se
*/

#include <cstring>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <unordered_map>
#include <vector>

#include <dirent.h>
#include <sys/types.h>

#include "AntiVirus.hpp"
#include "Constants.hpp"

std::unordered_map<std::string, std::vector<unsigned int>>
AntiVirus::readSignatureFile() {
    std::unordered_map<std::string, std::vector<unsigned int>> signatures;
    std::ifstream fileReader(signaturesFilePath);

    std::string line;
    while (std::getline(fileReader, line)) {
        std::istringstream stream(line);
        std::string signatureName, signatureData;
        std::getline(stream, signatureName, Constants::SIGNATURE_SEPARATOR);

        if (signatureName.length() > 32 || signatureName.length() == 0) {
            continue;
        }

        std::getline(stream, signatureData, Constants::SIGNATURE_SEPARATOR);
        const std::string::size_type signatureDataLength =
            signatureData.length();

        if(signatureDataLength == 0) {
            continue; 
        }

        const unsigned int byteCount = signatureDataLength / 2;

        if (byteCount > byteCountMax) {
            byteCountMax = byteCount;
        }

        std::vector<unsigned int> bytes(byteCount);
        for (unsigned int i = 0, j = 0; i < signatureDataLength; i += 2, j++) {
            const std::string sub = signatureData.substr(i, 2);
            const unsigned int byte = std::stoul(sub, nullptr, 16);
            bytes[j] = byte;
        }

        signatures[signatureName] = bytes;
    }

    fileReader.close();
    return signatures;
}

void AntiVirus::processDirectory(const std::string &directory) {
    DIR *dirp = opendir(directory.c_str());
    struct dirent *ent;

    while ((ent = readdir(dirp)) != NULL) {
        if (((std::strcmp(ent->d_name, Constants::CURRENT_DIRECTORY)) != 0) &&
            (std::strcmp(ent->d_name, Constants::PREVIOUS_DIRECTORY) != 0)) {
            const std::string fullFilePath = directory + Constants::DIRECTORY_SEPARATOR + ent->d_name;
            if (ent->d_type == DT_REG) {
                checkFile(fullFilePath);
            } else if (ent->d_type == DT_DIR) {
                processDirectory(fullFilePath);
            }
        }
    }
}

void AntiVirus::checkFile(const std::string &filePath) {
    std::ifstream fileReader(filePath);

    std::vector<unsigned int> fileBytes(byteCountMax);

    for (unsigned int i = 0; i < byteCountMax; i++) {
        if (fileReader.eof()) {
            break;
        }
        fileBytes[i] = fileReader.get();
    }

    for (auto &sig : signatureMap) {
        bool match = true;
        for (unsigned int i = 0; i < sig.second.size(); i++) {
            if (sig.second[i] != fileBytes[i]) {
                match = false;
                break;
            }
        }
        if (match) {
            logWriter << filePath << " matches signature " << sig.first << "\n";
        }
    }

    fileReader.close();
}
