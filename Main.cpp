/*
Gustav Karlsson - gustav.ae.karlsson@gmail.com
Claes Nyman - clny17@student.bth.se
*/

#include "AntiVirus.hpp"
#include "Constants.hpp"
#include <cstring>
#include <iostream>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

bool existsAndReadable(const std::string filePath) {
    const unsigned int err = access(filePath.c_str(), F_OK | R_OK);
    if (err != 0) {
        std::cerr << "Error accessing '" << filePath << "': ";
        switch (errno) {
        case EACCES:
            std::cerr << "Permission denied.\n";
            break;
        case ENOENT:
            std::cerr << "Does not exist.\n";
            break;
        }
        return false;
    }
    return true;
}

int main(int argc, char *argv[]) {
    if (argc != 5) {
        std::cout << argv[0]
                  << " --directory <directory> --signatures <signatures>\n";
        std::cout << "Output will be written to '" << Constants::LOG_FILE_NAME << "'\n";
        return 1;
    }
    std::string directory, signaturesFilePath;
    for (int i = 1; i < argc; i++) {
        if (std::strcmp(argv[i], "--directory") == 0) {
            directory = argv[i + 1];
            if (directory.back() == Constants::DIRECTORY_SEPARATOR) {
                directory = directory.substr(0, directory.length() - 1);
            }
        }
        if (std::strcmp(argv[i], "--signatures") == 0) {
            signaturesFilePath = argv[i + 1];
        }
    }

    if (!(existsAndReadable(directory) &&
          existsAndReadable(signaturesFilePath))) {
        return 1;
    }

    AntiVirus av(directory, signaturesFilePath);
    av.processDirectory(directory);
}
