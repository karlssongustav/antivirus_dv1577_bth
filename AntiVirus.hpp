/*
Gustav Karlsson - gustav.ae.karlsson@gmail.com
Claes Nyman - clny17@student.bth.se
*/

#include <fstream>
#include <sstream>
#include <string>
#include <unordered_map>
#include <vector>

#include "Constants.hpp"

class AntiVirus {
  private:
    std::string directory;
    std::string signaturesFilePath;

    unsigned int byteCountMax;
    std::unordered_map<std::string, std::vector<unsigned int>> signatureMap;
    std::ofstream logWriter;

    std::unordered_map<std::string, std::vector<unsigned int>> readSignatureFile();

    void checkFile(const std::string &filePath);

  public:
    AntiVirus(std::string &directory_, std::string &signaturesFilePath_)
        : directory(directory_), signaturesFilePath(signaturesFilePath_),
          byteCountMax(0), signatureMap(readSignatureFile()),
          logWriter(Constants::LOG_FILE_NAME){};
    ~AntiVirus() { logWriter.close(); };

    void processDirectory(const std::string &directory);
};
