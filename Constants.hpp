#ifndef CONSTANTS_HPP
#define CONSTANTS_HPP

#include <string>

namespace Constants {
const char *const LOG_FILE_NAME = "dv1577.log";
const char *const PREVIOUS_DIRECTORY = "..";
const char *const CURRENT_DIRECTORY = ".";
const char DIRECTORY_SEPARATOR = '/';
const char SIGNATURE_SEPARATOR = '=';
}; // namespace Constants

#endif
